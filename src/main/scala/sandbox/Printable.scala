package sandbox

trait Printable[A] {
    def format(value: A): String
}

object PrintableInstances {

    implicit val stringWriter = new Printable[String] {
        def format(value: String): String = value
    }

    implicit val intWriter = new Printable[Int] {
        def format(value: Int) = value.toString
    }
}

object Printable {
    def format[A](value: A)(implicit formatter: Printable[A]) = formatter.format(value);
    def print[A](value: A)(implicit formatter: Printable[A]) = println(format(value))
}

object PrintableSyntax {
    implicit class PrintableOps[A](value: A) {
        def format(implicit formatter: Printable[A]) = formatter.format(value)
        def print(implicit formatter: Printable[A]): Unit = Printable.print(value)
    }
}
