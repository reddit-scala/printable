package sandbox

case class Cat(name: String, age: Int, color: String)

object Cat {
    implicit val catFormatter: Printable[Cat] = new Printable[Cat] {
        def format(value: Cat): String = s"${value.name} is a ${value.age}-year-old ${value.color} cat"
    }
}